The "SASworkFlow.py" is the dags file, usually kept in the "/airflow/dags" path.

Before uploading the below files in the respective folder make sure you have root access:
One can use "sudo su" command to elevate to root access.

The "MSAP.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path
The "MSAP2.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path
The "MSAP3.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path
The "MSAP4.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path
The "MSAP5.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path
The "ConditionalTask.py" is the supporting file, usually kept in the "/airflow-materials/data-pipelines" path

Since this is a prototype and we still do not have any inputs, I have created the following dummy .csv files in the path shown below:
"airflow/dags/inputs" where "inputs" folder was manually created using "mkdir inputs" command
Dummy .csv files are

PreparatoryData.csv
DP1.csv
DP2.csv
DP3.csv
DP4PQ.csv
DP5PQ.csv
StellarModels.csv

Apart from this go to the Airflow user interface go to "Admin-->Connections-->Create", we will create the connection ID for all the File sensors as shown below:

1) Check PreparatoryData
task_id = "CheckPreparatoryData"
connection_type = "file(path)"
fs_conn_id = "CheckPreparatoryData"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

2) Check DP1
task_id = "CheckDP1"
connection_type = "file(path)"
fs_conn_id = "CheckDP1"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

3) Check DP2
task_id = "CheckDP2"
connection_type = "file(path)"
fs_conn_id = "CheckDP2"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

4) Check DP3
task_id = "CheckDP3"
connection_type = "file(path)"
fs_conn_id = "CheckDP3"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

5) Check DP4PreviousQuarter
task_id = "CheckDP4PreviousQuarter"
connection_type = "file(path)"
fs_conn_id = "CheckDP4PreviousQuarter"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

6) Check DP5PreviousQuarter
task_id = "CheckDP5PreviousQuarter"
connection_type = "file(path)"
fs_conn_id = "CheckDP5PreviousQuarter"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

7) Check Corrected Light Curves
task_id = "CheckCorrectedLightCurves"
connection_type = "file(path)"
fs_conn_id = "CheckCorrectedLightCurves"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

8) Check DP3 Output
task_id = "CheckDP3Out"
connection_type = "file(path)"
fs_conn_id = "CheckDP3Out"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

9) Check DP4 Output
task_id = "CheckDP4Out"
connection_type = "file(path)"
fs_conn_id = "CheckDP4Out"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

10) Classic Stellar Parameters
task_id = "ClassicStellarParameters"
connection_type = "file(path)"
fs_conn_id = "ClassicStellarParameters"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"

11) Check DP5 Output
task_id = "CheckDP5Out"
connection_type = "file(path)"
fs_conn_id = "CheckDP5Out"
extra = "{"path": "/home/vagrant/airflow/dags/inputs/"}"


Once these changes are done, everything is set up for the code to work fine.