#importing libraries
import pandas as pd
from datetime import datetime as dt
import os
#Set Local Directory
#Local_dir = os.getcwd()
Local_dir= '/home/vagrant/airflow/dags/inputs/'

def main():
    CorrectedLightCurveRead = pd.read_csv(os.path.join(Local_dir, 'Corrected_Light_Curves','Corrected_Light_Curves.csv'), delimiter = ',')
    PreparatoryDataRead = pd.read_csv(os.path.join(Local_dir, 'PreparatoryData','PreparatoryData.csv'), delimiter = ',')
    DP3OUT = CorrectedLightCurveRead.rename(columns = {'TimeStep':'frequency_of_maxpower_Vmax', 'Luminosity': 'large_frequency_separation_DeltaV'})
    #Check if a folder exists, else create one
    if not os.path.exists(os.path.join(Local_dir, 'DP3OUT')):
        os.mkdir('DP3OUT')
    DP3OUT.to_csv(os.path.join(Local_dir, 'DP3OUT','DP3OUT.csv'), index = False)

if __name__ == '__main__':
    main()