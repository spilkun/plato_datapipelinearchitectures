#importing libraries
import pandas as pd
from datetime import datetime as dt
import os
#Set a local directory
Local_dir = '/home/vagrant/airflow/dags/inputs/'
#Local_dir = os.getcwd()

def main():
    #reading the input files
    Preparatory_data = pd.read_csv(os.path.join(Local_dir, 'PreparatoryData','PreparatoryData.csv'), delimiter = ',')
    DP1 = pd.read_csv(os.path.join(Local_dir, 'DP1','DP1.csv'), delimiter = ',')
    DP2 = pd.read_csv(os.path.join(Local_dir, 'DP2','DP2.csv'), delimiter = ',')
    DP4PQ = pd.read_csv(os.path.join(Local_dir, 'DP4PQ','DP4PQ.csv'), delimiter = ',')
    
    #Check if a folder exists, else create one
    if not os.path.exists(os.path.join(Local_dir, 'Corrected_Light_Curves')):
        os.mkdir('Corrected_Light_Curves')
    
    #Generating the Outputs
    Corrected_Light_Curves = DP1.rename(columns = {'TimeStep':'TimeStep', 'Luminosity': 'Luminosity'})
    Corrected_Light_Curves.to_csv(os.path.join(Local_dir, 'Corrected_Light_Curves','Corrected_Light_Curves.csv'), index = False)

if __name__ == '__main__':
        main()