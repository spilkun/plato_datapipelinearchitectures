##################import libraries
import pandas as pd
from datetime import datetime as dt
import os

#set up the local directory
#Local_dir = os.getcwd()
Local_dir = '/home/vagrant/airflow/dags/inputs/'

def main():
    PreparatoryDataRead = pd.read_csv(os.path.join(Local_dir, 'PreparatoryData','PreparatoryData.csv'), delimiter = ',')
    DP3OUTRead = pd.read_csv(os.path.join(Local_dir, 'DP3OUT','DP3OUT.csv'), delimiter = ',')
    DP4OutRead = pd.read_csv(os.path.join(Local_dir, 'DP4OUT','DP4OUT.csv'), delimiter = ',')
    IDP123LOGGVARLCread = pd.read_csv(os.path.join(Local_dir, 'IDP_123_LOGG-VARLC','IDP_123_LOGG-VARLC.csv'), delimiter = ',')
    if not os.path.exists(os.path.join(Local_dir, 'ClassicStellarParameters')):
        os.mkdir('ClassicStellarParameters')
    DP3OUTRead.to_csv(os.path.join(Local_dir, 'ClassicStellarParameters','ClassicStellarParameters.csv'), index = False)

if __name__ == '__main__':
    main()