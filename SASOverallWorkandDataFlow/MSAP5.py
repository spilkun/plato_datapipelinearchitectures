#############################importing Libraries
import pandas as pd
from datetime import datetime as dt
import os

#set up the local directory
#Local_dir = os.getcwd()
Local_dir = "/home/vagrant/airflow/dags/inputs/"

def main():
    PreparatoryDataRead = pd.read_csv(os.path.join(Local_dir, 'PreparatoryData','PreparatoryData.csv'), delimiter = ',')
    ClassicStellarParametersRead = pd.read_csv(os.path.join(Local_dir, 'ClassicStellarParameters','ClassicStellarParameters.csv'), delimiter = ',')
    DP3OUTRead = pd.read_csv(os.path.join(Local_dir, 'DP3OUT','DP3OUT.csv'), delimiter = ',')
    DP4OUTRead = pd.read_csv(os.path.join(Local_dir, 'DP4OUT','DP4OUT.csv'), delimiter = ',')
    IDP123LOGGVARLCread = pd.read_csv(os.path.join(Local_dir, 'IDP_123_LOGG-VARLC','IDP_123_LOGG-VARLC.csv'), delimiter = ',')
    DP2Read = pd.read_csv(os.path.join(Local_dir, 'DP2','DP2.csv'), delimiter = ',')
    StellarModelsRead = pd.read_csv(os.path.join(Local_dir, 'StellarModels','StellarModels.csv'), delimiter = ',')
    if not os.path.exists(os.path.join(Local_dir, 'DP5OUT')):
        os.mkdir('DP5OUT')
    StellarModelsRead.to_csv(os.path.join(Local_dir, 'DP5OUT','DP5OUT.csv'), index = False)

if __name__ == '__main__':
    main()