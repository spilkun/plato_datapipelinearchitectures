#import libraries#
from airflow import DAG
from datetime import datetime
# These are the different operators we use
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.sensors.file_sensor import FileSensor
from airflow.utils.trigger_rule import TriggerRule
from airflow.operators.python_operator import PythonOperator
#MSAP to MSAP5 are python files created manually to read data from a given input#
import MSAP
import MSAP2
import MSAP3
import MSAP4
import MSAP5
import ConditionalTask

#defining default arguements
default_args = {
				"start_date" :datetime(2020, 11, 11),
				"owner" : "airflow"
				}

#instantiate DAG object
with DAG(dag_id = "SASworkFlow", schedule_interval ="@daily", default_args = default_args, catchup =False) as dag:
	Start = BashOperator(task_id = "Start", bash_command = 'echo "Start \n Checking Inputs"')
	#we first define the operators to check inputs
	#Check for Preparatory data
	CheckPreparatoryData = FileSensor(task_id = "CheckPreparatoryData", fs_conn_id = "CheckPreparatoryData", filepath = "PreparatoryData.csv", poke_interval=5)
	PreparatoryDataExists = BashOperator(task_id = "PreparatoryDataExists", bash_command = 'echo "Preparatory Data Exists"')

	#Check for Data Product 1
	CheckDP1 = FileSensor(task_id = "CheckDP1", fs_conn_id = "CheckDP1", filepath = "DP1.csv", poke_interval=5)
	DP1Exists = BashOperator(task_id = "DP1Exists", bash_command = 'echo "DP1 Exists"')
	#Check for Data Product 2
	CheckDP2 = FileSensor(task_id = "CheckDP2", fs_conn_id = "CheckDP2", filepath = "DP2.csv", poke_interval=5)
	DP2Exists = BashOperator(task_id = "DP2Exists", bash_command = 'echo "DP2 Exists"')

	#Check for Data Product 3
	CheckDP3 = FileSensor(task_id = "CheckDP3", fs_conn_id = "CheckDP3", filepath = "DP3.csv", poke_interval=5)
	DP3Exists = BashOperator(task_id="DP3Exists", bash_command = 'echo "DP3 Exists"')

	#Check for Data Product 4 of the previous quarter
	CheckDP4PreviousQuarter = FileSensor(task_id= "CheckDP4PreviousQuarter", fs_conn_id = "CheckDP4PreviousQuarter", filepath = "DP4PQ.csv", poke_interval=5)
	DP4PQExists = BashOperator(task_id= "DP4PQExists", bash_command = 'echo "DP4 from Previous Quarter Exists"')

	#Check for Data Product 5 of the previous quarter
	CheckDP5PreviousQuarter = FileSensor(task_id="CheckDP5PreviousQuarter", fs_conn_id = "CheckDP5PreviousQuarter", filepath = "DP5PQ.csv", poke_interval = 5)
	DP5PQExists = BashOperator(task_id = "DP5PQExists", bash_command = 'echo "DP5 from Previous Quarter Exists"')
	#Once all inputs are checked we would print a message indicating completion of input checks
	InitialInputCheckup = BashOperator(task_id = "InitialInputCheckup", bash_command = 'echo "Initial Inputs Check up Completed !!"')

	#We then carry out the first MSAP1 Operation, then check if output is produced, and print a message indicating its availability
	MSAP1 = PythonOperator(task_id="MSAP1", python_callable=MSAP.main)
	CheckCorrectedLightCurves = FileSensor(task_id="CheckCorrectedLightCurves", fs_conn_id = "CheckCorrectedLightCurves", filepath = "Corrected_Light_Curves.csv", poke_interval = 5)
	CorrectedLightCurvesExists = BashOperator(task_id ="CorrectedLightCurvesExists", bash_command = 'echo "Corrected Light Curves csv file exists"')

	#We then carry out the MSAP3 Operation, then check if output is produced, and print a message indicating its availability
	MSAP3 = PythonOperator(task_id="MSAP3", python_callable = MSAP3.main)
	CheckDP3Out = FileSensor(task_id = "CheckDP3Out", fs_conn_id = "CheckDP3Out", filepath = "DP3OUT.csv", poke_interval = 5)
	DP3OutExists = BashOperator(task_id="DP3OutExists", bash_command = 'echo "DP3 Output Exists"')
	#We then carry out the MSAP4 Operation, then check if output is produced, and print a message indicating its availability
	MSAP4 = PythonOperator(task_id =  "MSAP4", python_callable = MSAP4.main)
	CheckDP4Out= FileSensor(task_id = "CheckDP4Out", fs_conn_id = "CheckDP4Out", filepath = "DP4OUT.csv", poke_interval = 5)
	DP4OutExists = BashOperator(task_id= "DP4Exists", bash_command = 'echo "DP4 Output Exists"')

	#We then carry out the conditional Operation, here the condition true or false is executed by the presence of Oscillations Detection .csv file
	ConditionalTask = PythonOperator(task_id = "ConditionalTask", python_callable = ConditionalTask.main)

	MSAP2 = PythonOperator(task_id = "MSAP2", python_callable = MSAP2.main, trigger_rule = 'one_success')
	ClassicStellarParameters = FileSensor(task_id = "ClassicStellarParameters", fs_conn_id = "ClassicStellarParameters", filepath = "ClassicStellarParameters.csv", poke_interval =5)
	ClassicStellarParametersExists = BashOperator(task_id = "ClassicStellarParametersExists", bash_command = 'echo "Classic Stellar Parameters Exists"')

	#Given condition is False we then carry out the MSAP5 Operation, then check if output is produced, and print a message indicating its availability
	ConditionFailed = BashOperator(task_id = "ConditionFailed", bash_command = 'echo "Condition Failed"', trigger_rule = 'one_failed')

	MSAP5 = PythonOperator(task_id = "MSAP5", python_callable = MSAP5.main, trigger_rule = 'all_done')
	CheckDP5Out = FileSensor(task_id = "CheckDP5Out", fs_conn_id = "CheckDP5Out", filepath = "DP5OUT.csv", poke_interval = 5)
	DP5OutExists = BashOperator(task_id = "DP5OutExists", bash_command = 'echo "DP5 exists"')

	#Finally once all tasks are completed we execute the stop command
	Stop = BashOperator(task_id = "Stop", bash_command = 'echo "Stop"')

	#defining Dependencies

	Start >> CheckPreparatoryData
	CheckPreparatoryData >> PreparatoryDataExists
	Start >> CheckDP1
	CheckDP1 >> DP1Exists
	Start >> CheckDP2
	CheckDP2 >> DP2Exists
	Start >> CheckDP3
	CheckDP3 >> DP3Exists
	Start >> CheckDP4PreviousQuarter
	CheckDP4PreviousQuarter >> DP4PQExists
	Start >> CheckDP5PreviousQuarter
	CheckDP5PreviousQuarter >> DP5PQExists

	PreparatoryDataExists >> InitialInputCheckup
	DP1Exists >> InitialInputCheckup
	DP2Exists >> InitialInputCheckup
	DP3Exists >> InitialInputCheckup
	DP4PQExists >> InitialInputCheckup
	DP5PQExists >> InitialInputCheckup

	InitialInputCheckup >> MSAP1
	MSAP1 >> CheckCorrectedLightCurves
	CheckCorrectedLightCurves >> CorrectedLightCurvesExists

	CorrectedLightCurvesExists >> MSAP3
	MSAP3 >> CheckDP3Out
	CheckDP3Out >> DP3OutExists
	CorrectedLightCurvesExists >> MSAP4
	MSAP4 >> CheckDP4Out
	CheckDP4Out >> DP4OutExists

	DP3OutExists >> ConditionalTask
	DP4OutExists >> ConditionalTask
	ConditionalTask >> MSAP2 >> ClassicStellarParameters >> ClassicStellarParametersExists >> MSAP5 >> CheckDP5Out >> DP5OutExists >> Stop
    ConditionalTask >> ConditionFailed >> MSAP5 >> CheckDP5Out >> DP5OutExists >> Stop
