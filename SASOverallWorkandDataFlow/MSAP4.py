#import libraries
import pandas as pd
from datetime import datetime as dt
import os

#set up the local directory
#Local_dir = os.getcwd()
Local_dir = '/home/vagrant/airflow/dags/inputs/'
#Check if a folder exists, else create one


if not os.path.exists(os.path.join(Local_dir, 'DP4OUT')):
    os.mkdir('DP4OUT')
if not os.path.exists(os.path.join(Local_dir, 'IDP_123_LOGG-VARLC')):
    os.mkdir('IDP_123_LOGG-VARLC')
#reading inputs
CorrectedLightCurvesRead = pd.read_csv(os.path.join(Local_dir, 'Corrected_Light_Curves','Corrected_Light_Curves.csv'), delimiter = ',')
PreparatoryDataRead = pd.read_csv(os.path.join(Local_dir, 'PreparatoryData','PreparatoryData.csv'), delimiter = ',')
DP1Read = pd.read_csv(os.path.join(Local_dir, 'DP1','DP1.csv'), delimiter = ',')
DP2Read = pd.read_csv(os.path.join(Local_dir, 'DP2','DP2.csv'), delimiter = ',')
DP3Read = pd.read_csv(os.path.join(Local_dir, 'DP3','DP3.csv'), delimiter = ',')
DP4PQRead = pd.read_csv(os.path.join(Local_dir, 'DP4PQ','DP4PQ.csv'), delimiter = ',')
DP5PQRead = pd.read_csv(os.path.join(Local_dir, 'DP5PQ','DP5PQ.csv'), delimiter = ',')
           
DP4OUT = DP4PQRead.to_csv(os.path.join(Local_dir, 'DP4OUT','DP4OUT.csv'), index = False)
IDP123LOGGVARLC_Out = DP4PQRead.to_csv(os.path.join(Local_dir, 'IDP_123_LOGG-VARLC','IDP_123_LOGG-VARLC.csv'), index = False)